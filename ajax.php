<?php

include('./vendor/autoload.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if($_POST['action'] == 'podbor_po_foto') {
    $email = new PHPMailer();
    $email->SetFrom('robot@osveti.su', 'Магазин "Освети"'); //Name is optional
    $email->Subject   = 'Заявка с формы "Подбор по фото"';
    $email->Body      = 'Имя: '. $_POST['name'] . ' Телефон: ' . $_POST['telephone'] . ' Почта: ' . $_POST['email'];
    $email->AddAddress( 'as@pointer.global' );

    $file_to_attach = $_FILES['file']['tmp_name'];

    $email->AddAttachment( $file_to_attach , $_FILES['file']['name'] );

    return $email->Send();
}
if($_POST['action'] == 'filter_form') {
    $post = $_POST;
    $url = $post['url'];
    if($post['get_sort_order']) {
        $url .= '&' . $post['get_sort_order'];
    }
    unset($post['get_sort_order']);
    unset($post['action']);
    unset($post['url']);
    $get = '';
    if(count($post)) {
        $get .= '&filter=';
        foreach($post as $attr => $status) {           
            if(($attr == 'price_ot' || $attr == 'price_do')) {
                if($status == 0) {
                    continue;
                }
                $get .= $attr . ':' . $status. '.';
            } else {
                $get .= $attr . '.';
            }
            
        }
        $get = substr($get,0,-1);
    }
    header('Location: ' . $url . $get);    
}