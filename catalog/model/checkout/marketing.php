<?php
class ModelCheckoutMarketing extends Model {
	public function getMarketingByCode($code) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "marketing WHERE code = '" . $this->db->escape($code) . "'");

		return $query->row;
	}
	public function getAllMarketings() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "marketing");

		return $query->rows;
	}
}