<?php
class ControllerCheckoutMarketing extends Controller {
	public function index() {
        $this->load->model('checkout/marketing');
        $this->request->get['tracking'];
        $promotion = $this->model_checkout_marketing->getMarketingByCode($this->request->get['tracking']);
        $promotion['description'] = html_entity_decode($promotion['description']);
        $data['promo'] = $promotion;
        //var_dump($data);
        $data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
        $this->response->setOutput($this->load->view('checkout/marketing', $data));
    }
}