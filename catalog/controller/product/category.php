<?php
class ControllerProductCategory extends Controller {
	public function index() {
		$this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
			$filter_arr = explode('.', $filter);
			$selected_filters = [];
			$filter_attr = [];			
			foreach($filter_arr as $f) {
				$tmp = explode(':', $f);
				$filter_attr[$tmp[0]][] = array('id' => $tmp[0], 'text' => str_replace('_',' ',$tmp[1]));
				if($tmp[0] == 'price_ot' || $tmp[0] == 'price_do') {
					$selected_filters[$tmp[0]] = $tmp[1];
				} else {
					$selected_filters[$f] = str_replace('_',' ',$f);
				}
			}
			$data['selected_filters'] = $selected_filters;
		} else {
			$filter = '';
			$data['selected_filters'] = [];
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
			$data['select_sort'] = $order; 
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
		}
		//var_dump($limit);
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path . $url)
					);
				}
			}
		} else {
			$category_id = 0;
		}

		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info) {
			$this->document->setTitle($category_info['meta_title']);
			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);

			$data['heading_title'] = $category_info['name'];

			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));

			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $category_info['name'],
				'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
			);
			$data['category_parent_id'] = intval($category_info['parent_id']);
			$data['category_name'] = $category_info['name'];
			$data['category_href'] = $this->url->link('product/category', 'path=' . $this->request->get['path']);

			if ($category_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$data['compare'] = $this->url->link('product/compare');

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['categories'] = array();

			$results = $this->model_catalog_category->getCategories($category_id);

			foreach ($results as $result) {
				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);
				$data['categories'][] = array(
					'name' => $result['name'],
					'count' => ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
				);
			}

			$data['main_categories'] = array();

			$results = $this->model_catalog_category->getCategories();

			foreach ($results as $result) {
				if($category_id == $result['category_id']) continue;
				$data['main_categories'][$result['category_id']] = array(
					'name' => $result['name'],
					'image' => $result['image'], 
					'href' => $this->url->link('product/category', 'path=' . $result['category_id'])
				);
			}
			$data['parent_category'] = $data['main_categories'][$data['category_parent_id']];
			//var_dump($data['category_parent_id']);
			//var_dump($data['main_categories']);
			$data['products'] = array();
			//var_dump($filter);
			$filter_data = array(
				'filter_attribute'  => $filter_attr,
				'filter_category_id' => $category_id,
				'limit' => $limit,
				'order' => $order,
				'sort' => $sort,
			);
			
			//$product_total = $this->model_catalog_product->getTotalProducts($filter_data);
			//var_dump($product_total);

		
			//var_dump($data['filter']);
			$results = $this->model_catalog_product->getProducts($filter_data);
			$product_ids = [];
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				/*$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url),
					'all'         => $result,
				);*/
				$product_ids[] = $result['product_id'];
				$data['products'][] = $result;
				$data['products'][array_key_last($data['products'])]['attributes'] = $this->model_catalog_product->getProductAttributes($result['product_id']);
				$data['products'][array_key_last($data['products'])]['images'] = $this->model_catalog_product->getProductImages($result['product_id']);
				$name = $data['products'][array_key_last($data['products'])]['name'];
				$new_name = stristr($name, $data['products'][array_key_last($data['products'])]['attributes'][1]['attribute'][2]['text'], true);								
				$data['products'][array_key_last($data['products'])]['name'] = $new_name;

			}
			$product_total = count($data['products']);
			$filter_attr = [];

			$filter_attr['Высота, мм'] = $this->model_catalog_product->getAttributes($category_id, 'Высота, мм', $product_ids);
			$filter_attr['Длина, мм'] = $this->model_catalog_product->getAttributes($category_id, 'Длина, мм', $product_ids);
			$filter_attr['Глубина, мм'] = $this->model_catalog_product->getAttributes($category_id, 'Глубина, мм', $product_ids);
			$filter_attr['Ширина, мм'] = $this->model_catalog_product->getAttributes($category_id, 'Ширина, мм', $product_ids);
			$filter_attr['Диаметр, мм'] = $this->model_catalog_product->getAttributes($category_id, 'Диаметр, мм', $product_ids);
			$filter_attr['Длина цепи, мм'] = $this->model_catalog_product->getAttributes($category_id, 'Длина цепи, мм', $product_ids);
			$filter_attr['Стиль'] = $this->model_catalog_product->getAttributes($category_id, 'Стиль', $product_ids);
			$filter_attr['Место применения'] = $this->model_catalog_product->getAttributes($category_id, 'Место применения', $product_ids);
			$filter_attr['Тип лампочки'] = $this->model_catalog_product->getAttributes($category_id, 'Тип лампочки', $product_ids);
			$filter_attr['Форма светильника'] = $this->model_catalog_product->getAttributes($category_id, 'Форма светильника', $product_ids);
			$filter_attr['Параметры плафонов'] = $this->model_catalog_product->getAttributes($category_id, 'Параметры плафонов', $product_ids);
			$filter_attr['Параметры арматуры'] = $this->model_catalog_product->getAttributes($category_id, 'Параметры арматуры', $product_ids);
			$filter_attr['Тип управления'] = $this->model_catalog_product->getAttributes($category_id, 'Тип управления', $product_ids);
			$filter_attr['Особенности'] = $this->model_catalog_product->getAttributes($category_id, 'Особенности', $product_ids);
			$filter_attr['Пульт ДУ'] = $this->model_catalog_product->getAttributes($category_id, 'Пульт ДУ', $product_ids);
			$filter_attr['Выключатель'] = $this->model_catalog_product->getAttributes($category_id, 'Выключатель', $product_ids);
			$filter_attr['Тип подвеса'] = $this->model_catalog_product->getAttributes($category_id, 'Тип подвеса', $product_ids);
			$filter_attr['Страна'] = $this->model_catalog_product->getAttributes($category_id, 'Страна', $product_ids);
			$filter_attr['Бренд'] = $this->model_catalog_product->getAttributes($category_id, 'Бренд', $product_ids);
			$filter_attr['Диммер'] = $this->model_catalog_product->getAttributes($category_id, 'Диммер', $product_ids);
			$filter_attr['Теги'] = $this->model_catalog_product->getAttributes($category_id, 'Теги', $product_ids);
			$filter_attr['Степень защиты'] = $this->model_catalog_product->getAttributes($category_id, 'Степень защиты', $product_ids);
			$filter_attr['Напряжение, V'] = $this->model_catalog_product->getAttributes($category_id, 'Напряжение, V', $product_ids);
			$filter_attr['Количество фаз'] = $this->model_catalog_product->getAttributes($category_id, 'Количество фаз', $product_ids);

			$data['filter'] = $filter_attr;
			//var_dump($data['products']);
			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
				);
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
			);

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');
			//var_dump($pagination);

			$data['pagination'] = $pagination->renderCatalog();
			//var_dump($data['pagination']);

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
			if ($page == 1) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id']), 'canonical');
			} else {
				$this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. $page), 'canonical');
			}
			
			if ($page > 1) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . (($page - 2) ? '&page='. ($page - 1) : '')), 'prev');
			}

			if ($limit && ceil($product_total / $limit) > $page) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page + 1)), 'next');
			}

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('product/category', $data));
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header'); 
			$data['get_filter'] = $this->request->get['filter'];
			$data['get_sort_order'] = $this->request->get['sort'] . '&' . $this->request->get['order'];
			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}
}
