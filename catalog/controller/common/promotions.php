<?php
class ControllerCommonPromotions extends Controller {
	public function index() {
        $this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));
        $data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
        $this->load->model('checkout/marketing');
		$marketing_info = $this->model_checkout_marketing->getAllMarketings();
		if($marketing_info) {
			foreach($marketing_info as $m) {
				$data['marketings'][] = $m; 
			}
		}        
        $this->load->model('catalog/product');
        $filter_data = array(
            'sort'               => 'RAND()',
            'limit'              => 1
        );
        $results = $this->model_catalog_product->getProducts($filter_data);
        $data['product'] = array_shift($results);
        $data['product']['attributes'] = $this->model_catalog_product->getProductAttributes($data['product']['product_id']);  
        $data['product']['images'] = $this->model_catalog_product->getProductImages($data['product']['product_id']);
        $name = $data['product']['name'];
        $new_name = stristr($name, $data['product']['attributes'][1]['attribute'][2]['text'], true);								
        $data['product']['name'] = $new_name;     
        //var_dump($data);     
        $this->response->setOutput($this->load->view('common/promotions', $data));
    }
}