<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}
		$this->load->model('checkout/marketing');
		$marketing_info = $this->model_checkout_marketing->getAllMarketings();
		if($marketing_info) {
			foreach($marketing_info as $m) {
				$data['marketings'][] = $m; 
			}
		}
		
		$this->load->model('catalog/category');
		$popular_categories = $this->model_catalog_category->getPopularCategories();
		if($popular_categories) {
			foreach($popular_categories as $c) {
				$data['popular_categories'][] = $c;
				//var_dump($c['image']);
				$data['popular_categories'][array_key_last($data['popular_categories'])]['href'] = $this->url->link('product/category', 'path=' . $c['category_id']); 			
			}
		}
		//var_dump($data['popular_categories']);
		
		$this->load->model('catalog/manufacturer');
		$manufacturers = $this->model_catalog_manufacturer->getManufacturers();
		if($manufacturers) {
			foreach($manufacturers as $m) {
				$data['manufacturers'][] = $m; 
			}
		}

		$this->load->model('catalog/product');
		$products = $this->model_catalog_product->getProducts(array('filter_attribute_name' => 'Эксклюзив', 'limit' => 12));
		if($products) {
			foreach($products as $p) {
				$data['exclusives'][] = $p;
				$data['exclusives'][array_key_last($data['exclusives'])]['attributes'] = $this->model_catalog_product->getProductAttributes($p['product_id']);
				//var_dump($result['product_id']);
				$data['exclusives'][array_key_last($data['exclusives'])]['images'] = $this->model_catalog_product->getProductImages($p['product_id']);
				$name = $data['exclusives'][array_key_last($data['exclusives'])]['name'];
				$new_name = stristr($name, $data['exclusives'][array_key_last($data['exclusives'])]['attributes'][1]['attribute'][2]['text'], true);								
				$data['exclusives'][array_key_last($data['exclusives'])]['name'] = $new_name; 
			}
		}
		$products = $this->model_catalog_product->getProducts(array('filter_attribute_name' => 'Новинка', 'limit' => 12));
		if($products) {
			foreach($products as $p) {
				$data['new_products'][] = $p;
				$data['new_products'][array_key_last($data['new_products'])]['attributes'] = $this->model_catalog_product->getProductAttributes($p['product_id']);
				//var_dump($result['product_id']);
				$data['new_products'][array_key_last($data['new_products'])]['images'] = $this->model_catalog_product->getProductImages($p['product_id']);
				$name = $data['new_products'][array_key_last($data['new_products'])]['name'];
				$new_name = stristr($name, $data['new_products'][array_key_last($data['new_products'])]['attributes'][1]['attribute'][2]['text'], true);								
				$data['new_products'][array_key_last($data['new_products'])]['name'] = $new_name;  
			}
		}
		$products = $this->model_catalog_product->getProducts(array('filter_attribute_name' => 'Распродажа', 'limit' => 12));
		if($products) {
			foreach($products as $p) {
				$data['sale'][] = $p;
				$data['sale'][array_key_last($data['sale'])]['attributes'] = $this->model_catalog_product->getProductAttributes($p['product_id']);
				//var_dump($result['product_id']);
				$data['sale'][array_key_last($data['sale'])]['images'] = $this->model_catalog_product->getProductImages($p['product_id']);
				$name = $data['sale'][array_key_last($data['sale'])]['name'];
				$new_name = stristr($name, $data['sale'][array_key_last($data['sale'])]['attributes'][1]['attribute'][2]['text'], true);								
				$data['sale'][array_key_last($data['sale'])]['name'] = $new_name;   
			}
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
