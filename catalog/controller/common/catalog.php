<?php
class ControllerCommonCatalog extends Controller {
	public function index() {
		$this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

        $data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

        $data['main_categories'] = array();

        $results = $this->model_catalog_category->getCategories();

        foreach ($results as $result) {
            $filter_data = array(
                'filter_category_id'  => $result['category_id'],
            );

            $data['main_categories'][] = array(
                'id' => $result['category_id'],
                'name' => $result['name'],
                'count' => ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                'image' => $result['image'],
                'image_mini' => $result['image_mini'],
                'image_for_filter' => $result['image_for_filter'], 
                'href' => $this->url->link('product/category', 'path=' . $result['category_id'])
            );
            $subcategories = [];
            $sc_results = $this->model_catalog_category->getCategories($result['category_id']);
            foreach ($sc_results as $r) {
                $subcategories[] = array(
                    'name' => $r['name'],
                    'href' => $this->url->link('product/category', 'path=' . $r['category_id'])
                );
            }
            $data['main_categories'][array_key_last($data['main_categories'])]['subcategories'] = $subcategories;
            //$materials = [];
            $filter_data = array(
                'filter_category_id' => $data['main_categories'][array_key_last($data['main_categories'])]['id'],
                'filter_attribute_name' => 'Виды материалов',
            );
            $materials = $this->model_catalog_product->getProductMaterials($result['category_id'], 'Виды материалов');
            /*if(!$materials[$attributes[1]['attribute'][20]['text']]) {
                $materials[$attributes[1]['attribute'][20]['text']] = $attributes[1]['attribute'][20]['text'];
            }*/        
            $data['main_categories'][array_key_last($data['main_categories'])]['materials'] = $materials;
            //echo"123";
            //var_dump($materials);
        }

        $this->response->setOutput($this->load->view('common/catalog', $data));
    }
}