if (jQuery(window).width() < 1000) {
    $(function () {
        $('.mobile-menu__list li span').click(function () {
            $(this).closest('li').find('ul').eq(0).slideToggle(500);
        });
    });
    $(function () {
        $('.mobile-menu__item span').on('click', function () {
            $(this).toggleClass('active');
        });
    });
  }
  
  $('.tabs-item-big-title').click(function () {
 $(this).closest('.tabs-item-big-title').next('.tabs-item-big-infowrapper').slideToggle();
  return false;
  });
  
   $('.tabs-item-big-title span').on('click', function () {
        $(this).toggleClass('active');
    });
  
  // $('.tabs-item-big-title span').on('click', function () {
  //   $('.tabs-item-big-title span').toggleClass('active');
  // });
  
   //Izbrannoe mob 
   if ($(window).width() < 1399) { 
   let countmob = document.getElementById("countizbrannoemob"),
    counter = 0;
  
  $(".addizbrannoe").on('click', function () {
      const $this = $(this);
   
      if  ($(this).hasClass("addplus"))  {
      $(this).removeClass('addplus').addClass('ss');
      counter -= 1;
      
      } else {
        $(this).addClass("addplus");
        $('.izbrannoecount').css('display', 'flex').hide().fadeIn(500);
        counter += 1;
      }
      countmob.innerHTML = counter;
});
   };
  
  //Izbrannoe
    if ($(window).width() > 1400) {  
let button = document.getElementById("countizbrannoe"),
    count = 0;
  
  $(".addizbrannoe").on('click', function () {
      const $this = $(this);
   
      if  ($(this).hasClass("addplus"))  {
      $(this).removeClass('addplus').addClass('ss');
      count -= 1;
      
      } else {
        $(this).addClass("addplus");
        $('.izbrannoecount').css('display', 'flex').hide().fadeIn(500);
        count += 1;
      }
      button.innerHTML = count;
});
    };

  // TABSFILTER
  
  $(function () {
  $('.filter-tabs__link').on('click', function () {
      $(this).toggleClass('active');
  });
  });
  
  $('.filter-tabs__link').click(function () {
  $(this).closest('div').find('div').eq(0).slideToggle();
  
  });
  
  // TABSFILTER
  
  $(document).ready(function () {
    const $item = $(".mobile-menu__item");
    $item.each(function () {
        const $target = $(this).find("span");
        $target.on("click", function () {
            const $parent = $(this).parent(".mobile-menu__item");
            $item.removeClass("mobile-menu__link--red");
            $parent.addClass("mobile-menu__link--red");
        });
    })
  });
  
  const submenuBtn = document.querySelectorAll(".mobile-menu__item span");
  const submenu = document.querySelectorAll(".mobile-menumini__list");
  submenuBtn.forEach(btn => {
    btn.addEventListener("click", () => {
        submenu.forEach(element => {
            element.classList.remove("mobile-menumini__list--open");
        });
        btn.nextElementSibling.classList.add("mobile-menumini__list--open");
    });
  });
  
  $('.all__show').click(function () {
    $('.mobile-menu__list-all').slideToggle(500);
    $('.all__show').hide();
    return false;
  });
  
  $('.all__show-none').click(function () {
    $('.mobile-menu__list-all').slideToggle(500);
    $('.all__show').show();
    return false;
  });
  
  var searchBlock = $('#moving_block');
  $(document).on('click', '.page-header__option-screach', function () {
    searchBlock.slideDown("slow");
    return false;
  });
  
  var searchBlock = $('#moving_block');
  $(document).on('click', '.moving__close', function () {
    searchBlock.hide("500");
    return false;
  });
  
  var searchBlockk = $('#moving_block-tablet');
  $(document).on('click', '.page-header__option-screach', function () {
    searchBlockk.slideDown("slow");
    return false;
  });
  
  var searchBlockk = $('#moving_block-tablet');
  $(document).on('click', '.moving__close', function () {
    searchBlockk.hide("500");
    return false;
  });
  
  var searchBlocks = $('#moving_block-desktop');
  $(document).on('click', '.page-header__option-screach', function () {
    searchBlocks.slideDown("slow");
    return false;
  });
  
  var searchBlocks = $('#moving_block-desktop');
  $(document).on('click', '.moving__close', function () {
    searchBlocks.hide("500");
    return false;
  });
  
  $('.intro__slide').slick({
    arrows: true,
    dots: true,
    swipeToSlide: true,
    slidesToShow: 1,
    infinite: true,
    responsive: [{
        breakpoint: 1000,
        settings: {
          arrows: true,
          dots: false,
          slidesToShow: 3,
          infinite: true,
          autoplay: false,
          swipeToSlide: true,
          variableWidth: true,
        }
      },
      {
        breakpoint: 3000,
        settings: {
          arrows: true,
          dots: false,
          slidesToShow: 6,
          infinite: true,
          swipeToSlide: true,
          autoplay: false,
          variableWidth: true,
        }
      }
    ] 
  });
  
   $('.mini__gllery-item').slick({
    arrows: true,
    dots: true,
    swipeToSlide: true,
    slidesToShow: 3,
    infinite: true,
    responsive: [{
        breakpoint: 1000,
        settings: {
          arrows: true,
          dots: false,
          swipeToSlide: true,
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 3000,
        settings: {
          arrows: true,
          dots: false,
          vertical: true,
          verticalSwiping: true,
          swipeToSlide: true,
          slidesToShow: 3,
        }
      }
    ]
  });
  
  
  $('.slide__wrapper').slick({
    arrows: true,
    dots: true,
    swipeToSlide: true,
    slidesToShow: 1,
    infinite: true,
    responsive: [{
        breakpoint: 1000,
        settings: {
          arrows: true,
          dots: false,
          swipeToSlide: true,
          slidesToShow: 3,
          infinite: true,
          autoplay: false,
          variableWidth: true,
        }
      },
      {
        breakpoint: 3000,
        settings: {
          arrows: true,
          dots: false,
          swipeToSlide: true,
          slidesToShow: 4,
          infinite: true,
          autoplay: false,
          variableWidth: true,
        }
      }
    ]
  });
  
  $('.catalog__icon-one').click(function () {
    $('.catalog__list-mini-one').slideToggle(500);
    this.classList.toggle('rotate')
    return false;
  });
  
  $('.catalog__icon-two').click(function () {
    $('.catalog__list-mini-two').slideToggle(500);
    this.classList.toggle('rotate')
    return false;
  });
  
  $('.slide__img-wrapper').slick({
  arrows: false,
  dots: true,
  slidesToShow: 1,
  swipeToSlide: false,
  infinite: true,
  responsive: [{
    breakpoint: 800,
    settings: {
      arrows: false,
      dots: true,
      slidesToShow: 1,
      swipeToSlide: false,
      infinite: true,
      autoplay: false,
    }
  },
  {
    breakpoint: 1400,
    settings: {
      arrows: false,
      dots: true,
      slidesToShow: 1,
      swipeToSlide: false,
      infinite: true,
      autoplay: false,
      variableWidth: true,
    }
  }, 
  {
    breakpoint: 3000,
    settings: {
      arrows: false,
      dots: true,
      swipeToSlide: false,
      slidesToShow: 1,
      infinite: true,
      autoplay: false,
      variableWidth: true,
    }
  }
  ]
  });
  
  $('.catalog__slide').slick({
    arrows: false,
    dots: false,
    slidesToShow: 1,
    swipeToSlide: true,
    infinite: true,
    responsive: [{
        breakpoint: 800,
        settings: {
          arrows: false,
          dots: false,
          swipeToSlide: true,
          slidesToShow: 3,
          infinite: true,
          autoplay: false,
          variableWidth: true,
        }
      },
      {
        breakpoint: 1400,
        settings: {
          arrows: true,
          swipeToSlide: true,
          dots: false,
          slidesToShow: 4,
          infinite: true,
          autoplay: false,
          variableWidth: true,
        }
      }, 
      {
        breakpoint: 3000,
        settings: {
          arrows: true,
          dots: false,
          swipeToSlide: true,
          slidesToShow: 6,
          infinite: true,
          autoplay: false,
          variableWidth: true,
        }
      }
    ]
  });
  
  $('.catalog__intro-slide-wrapper').slick({
    arrows: true,
    dots: false,
    slidesToShow: 6,
    swipeToSlide: true,
    infinite: true,
  });
  
  $( ".content__tabs" ).on( "click", "li" , function() {
  $( this ).toggleClass( "content__tabs-active" );
  });
  
  $('.content__tabs-linkwrapper-all a').click(function () {
    $('.content__tabs-wrapper_all').slideToggle(500);
    $('.content__tabs-wrapper_all').css('display','flex');
    $('.content__tabs-linkwrapper-all').hide();
    $('.content__tabs-linkwrapper-hide').show();
    return false;
  });
  
  $('.content__tabs-linkwrapper-hide a').click(function () {
  $('.content__tabs-wrapper_all').slideToggle(500);
  $('.content__tabs-linkwrapper-hide').hide();
  $('.content__tabs-linkwrapper-all').show();
  return false;
  });
  
  const menu = document.querySelector('.mobile-menu__btn');
  menu.addEventListener("click", function(){
  document.body.classList.toggle('body-hidden'); 
  });
  
  $(document).ready(function() {
  $('.minus').click(function () {
      var $input = $(this).parent().find('input');
      var count = parseInt($input.val()) - 1;
      count = count < 1 ? 1 : count;
      $input.val(count);
      $input.change();
      return false;
  });
  $('.plus').click(function () {
      var $input = $(this).parent().find('input');
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      return false;
  });
  });
  
  $('.sravnenie__slider-wrapper').slick({
  arrows: true,
  dots: true,
  slidesToShow: 1,
  infinite: true,
  responsive: [{
      breakpoint: 1000,
      settings: {
        arrows: true,
        dots: false,
        slidesToShow: 3,
        infinite: true,
        autoplay: false,
        variableWidth: true,
        asNavFor:('.sravnenie-harakteristiki__wrapper'),
      }
    },
    {
      breakpoint: 3000,
      settings: {
        arrows: true,
        dots: false,
        slidesToShow: 4,
        infinite: true,
        autoplay: false,
        variableWidth: true,
        asNavFor:('.sravnenie-harakteristiki__wrapper'),
      }
    }
  ]
  });
  
  $('.sravnenie-harakteristiki__wrapper').slick({
  arrows: false,
  dots: false,
  slidesToShow: 1,
  infinite: true,
  responsive: [{
      breakpoint: 1000,
      settings: {
        arrows: false,
        dots: false,
        slidesToShow: 3,
        infinite: true,
        autoplay: false,
        variableWidth: true,
      }
    },
    {
      breakpoint: 3000,
      settings: {
        arrows: true,
        dots: false,
        slidesToShow: 4,
        infinite: true,
        autoplay: false,
        asNavFor:('.sravnenie__slider-wrapper, .sravnenie-harakteristiki__wrapper'),
        variableWidth: true,
      }
    }
  ]
  });
  
  $('body').on('click', '.password-control', function(){
  if ($('#password-input').attr('type') == 'password'){
    $(this).addClass('view');
    $('#password-input').attr('type', 'text');
  } else {
    $(this).removeClass('view');
    $('#password-input').attr('type', 'password');
  }
  return false;
  });
  
  $('.page__header-basket-tablet').click(function() {
    $('.basket__popup-wrapper').fadeIn();
    return false;
  });	
  
  $('.log__popup-close').click(function() {
    $(this).parents('.basket__popup-wrapper').fadeOut();
    return false;
  });		
  
  $(document).keydown(function(e) {
    if (e.keyCode === 27) {
      e.stopPropagation();
      $('.basket__popup-wrapper').fadeOut();
    }
  });
  
  $('.basket__popup-wrapper').click(function(e) {
    if ($(e.target).closest('.basket__popup-wrapper').length == 0) {
      $(this).fadeOut();					
    }
  });
  
  $('.page-header__option-log').on('click', function(){
  $('.log__popup-wrapper').css('display', 'flex').hide().fadeIn(500);
  });
  
  $('.log__popup-close').on('click', function(){
  $('.log__popup-wrapper').fadeOut(500);
  });
  
  $('.log__popup-zabil').on('click', function(){
  $('.pass__popup-wrapper').css('display', 'flex').hide().fadeIn(500);
  });
  
  $('.log__popup-close').on('click', function(){
  $('.pass__popup-wrapper').fadeOut(500);
  });
  
  $('.log__popup-go-link').on('click', function(){
  $('.reg__popup-wrapper').css('display', 'flex').hide().fadeIn(500);
  });
  
  $('.log__popup-close').on('click', function(){
  $('.reg__popup-wrapper').fadeOut(500);
  });
  
  
  $('.sravnenie-option__item span span').click(function () {
  $(this).closest('li').find('.sravnenie-harakteristiki__wrapper').eq(0).slideToggle();
  });
  
  $(function() {
    function maskPhone() {
      var country = $('#country option:selected').val();
      switch (country) {
        case "ru":
          $("#phone").mask("+7(999) 999-99-99");
          break;
        case "ua":
          $("#phone").mask("+380(999) 999-99-99");
          break;
        case "by":
          $("#phone").mask("+375(999) 999-99-99");
          break;          
      }    
    }
    maskPhone();
    $('#country').change(function() {
      maskPhone();
    });
  });
  
  $(function() {
    function maskPhone() {
      var country = $('#country option:selected').val();
      switch (country) {
        case "ru":
          $("#phones").mask("+7(999) 999-99-99");
          break;
        case "ua":
          $("#phones").mask("+380(999) 999-99-99");
          break;
        case "by":
          $("#phones").mask("+375(999) 999-99-99");
          break;          
      }    
    }
    maskPhone();
    $('#countrytwo').change(function() {
      maskPhone();
    });
  });
  
    $("select").change(function(){
        var divId = $(this).find(":selected").attr("data-div-id");
        $(".container").hide();
        $("#" + divId).show();
    });
    
  if ($(window).width() > 1400) {  
      
      
      $('.mobile-menu__item').hover(
        function(ev){
            var li = $(this);
            li.parents('.mobile-menu__item').find('>.mobile-menumini__list').hide();
            if ( ! li.find('.mobile-menu__item > .mobile-menumini__list:visible').length ) {
                li.find('>.mobile-menumini__list').show();
            }
        },
        function(){
            var li = $(this);
            li.find('>.mobile-menumini__list').hide();
            li.parents('li:first').find('>.mobile-menumini__list').show();
        }
    );
  };
  
  //sort_select
  function selectSort() {
    console.log($('#sort_select').val());
      if($('#sort_select').val()){
        document.location.href = $('#sort_select').val();
      }    
  };
  
  $('.page-header__option-message').on('click', function(){
    $('.message__popup-wrapper').css('display', 'flex').hide().fadeIn(500);
  });
  
  $('.log__popup-close').on('click', function(){
    $('.message__popup-wrapper').fadeOut(500);
  });	
      
  $('.page-header__option-tel').on('click', function(){
    $('.tel__popup-wrapper').css('display', 'flex').hide().fadeIn(500);
  });
  
  $('.log__popup-close').on('click', function(){
    $('.tel__popup-wrapper').fadeOut(500);
  });
      
  $('.page-header__option-city').on('click', function(){
    $('.city__popup-wrapper').css('display', 'flex').hide().fadeIn(500);
  });
  
  $('.log__popup-close').on('click', function(){
    $('.city__popup-wrapper').fadeOut(500);
  });
  
  
  //filter scroll
  
  $('.filter__go').click(function () {
    $('.filter__wrapper').slideToggle(500);
  $('.filter__go').hide(500);
  $('.filter__end').show(500);
  });
  
  $('.filter__end').click(function () {
    $('.filter__wrapper').slideToggle(500);
  $('.filter__end').hide(500);
  $('.filter__go').show(500);
  });
  
  // GOROD
  var tags = [ "Москва", "Санкт-Петербург", "Новосибирск", "Екатеринбург", "Нижний Новгород", "Казань", "Челябинск", "Омск", "Самара", "Ростов-на-дону", "Уфа", "Красноярск", "Пермь", "Воронеж", "Волгоград", "Саратов", "Краснодар", "Тольятти", "Тюмень", "Ижевск", "Барнаул", "Иркутск", "Ульяновск", "Хабаровск", "Владивосток", "Ярославль", "Махачкала", "Томск", "Оренбург", "Новокузнецк", "Кемерово", "Рязань", "Астрахань", "Набережные челны", "Пенза", "Липецк", "Тула", "Чебоксары", "Киров", "Калининград", "Курск", "Улан-Удэ", "Ставрополь", "Балашиха", "Магнитогорск", "Севастополь", "Тверь", "Иваново", "Брянск", "Сочи", "Белгород", "Нижний-Тагил", "Владимир", "Архангельск", "Сургут", "Чита", "Калуга", "Симферополь", "Смоленск", "Волжский", "Курган", "Орел", "Череповец", "Вологда", "Саранск", "Владикавказ", "Якутск", "Мурманск", "Подольск", "Тамбов", "Грозный"];
  $( "#autocomplete" ).autocomplete({
  source: function( request, response ) {
          var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
          response( $.grep( tags, function( item ){
              return matcher.test( item );
          }) );
          
      }
  });
  
  $('.city__submit').click(function () {
    event.preventDefault();
  var input = document.getElementById("autocomplete");
  var div = document.getElementById("cityactive");
  div.innerText = input.value;
   $('.city__popup-wrapper').fadeOut(500);
  });
  
     // VALID
  
     const form = document.getElementById('dostavka');
     const username = document.getElementById('name');
     const email = document.getElementById('email');
     
     function showError(input, message) {
       const formControl = input.parentElement;
       formControl.className = 'form-basket-content-input error';
       const small = formControl.querySelector('small');
       small.innerText = message;
     }
     
     function showSucces(input) {
       const formControl = input.parentElement;
       formControl.className = 'form-basket-content-input success';
     }
     
     function checkEmail(input) {
       const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       if(re.test(input.value.trim())) {
           showSucces(input)
       }else {
           showError(input,'Заполните поле');
       }
     }
     
     function checkRequired(inputArr) {
       inputArr.forEach(function(input){
           if(input.value.trim() === ''){
               showError(input,`${getFieldName(input)} is required`)
           }else {
               showSucces(input);
           }
       });
     }
     
     function checkLength(input, min ,max) {
       if(input.value.length < min) {
           showError(input, `${getFieldName(input)} must be at least ${min} characters`);
       }else if(input.value.length > max) {
           showError(input, `${getFieldName(input)} must be les than ${max} characters`);
       }else {
           showSucces(input);
       }
     }
     
     function getFieldName(input) {
       return input.id.charAt(0).toUpperCase() + input.id.slice(1);
     }
     
     form.addEventListener('submit',function(e) {
       e.preventDefault();
       checkLength(username,3,15);
       checkEmail(email);
     });
     